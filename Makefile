DKR_COMP = docker compose
DKR_EXEC = docker compose exec

up: .env
	$(DKR_COMP) up -d web db nginx

DB_NAME = postgres
DB_USER = postgres
DB_BACKUP_DIR = ./backup
DB_BACKUP_FILE = $(DB_BACKUP_DIR)/dump.sql.gz
DB_BACKUP_LATEST = $(DB_BACKUP_DIR)/$(shell ls -t1 $(DB_BACKUP_DIR))
DB_RS_SEQ_FILE = ./app/postgres/reset.sql

schema_update:
	$(DKR_EXEC) web php bin/console doctrine:schema:update --force
db_create:
	$(DKR_EXEC) web php bin/console doctrine:database:create --if-not-exists
db_backup:
	$(DKR_EXEC) -T db pg_dump -U $(DB_USER) -w $(DB_NAME) | gzip > $(DB_BACKUP_FILE)
db_restore: _db_restore db_fix_seq
_db_restore:
	cat $(DB_BACKUP_LATEST) | gunzip | $(DKR_EXEC) -T db psql -U $(DB_USER) $(DB_NAME)
db_fix_seq:
	$(DKR_EXEC) -T db psql -U postgres -Atq < $(DB_RS_SEQ_FILE) | $(DKR_EXEC) -T db psql -U postgres

web_install:
	$(DKR_EXEC) web composer install

crawl:
	$(DKR_COMP) run --rm crawler

download:
	$(DKR_COMP) run --rm downloader

upload:
	$(DKR_COMP) run --rm uploader

add_channel:
	$(DKR_COMP) run add_channel

LOG_DIR = `pwd`/logs
cron_all: cron_upload cron_backup cron_crawl cron_download

cron_upload:
	(crontab -l; echo "0 */4 * * * (date; make -C `pwd` upload) >> $(LOG_DIR)/upload.log 2>&1") | sort -r - | uniq - | crontab -

cron_backup:
	(crontab -l; echo "0 10 * * * make -C `pwd` db_backup") | sort -r - | uniq - | crontab -

cron_crawl:
	(crontab -l; echo "0 */12 * * * (date; make -C `pwd` crawl) >> $(LOG_DIR)/crawl.log 2>&1") | sort -r - | uniq - | crontab -

cron_download:
	(crontab -l; echo "30 */12 * * * (date; make -C `pwd` download) >> $(LOG_DIR)/download.log 2>&1") | sort -r - | uniq - | crontab -

.env:
	printf "UID=$(shell id -u)\n\
GID=$(shell id -g)" > .env
