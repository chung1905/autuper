<?php

namespace App\Form;

use App\Entity\GoogleClient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoogleClientType extends AbstractType
{
    public function __construct(
//        private BaseDataMapper $dataMapper
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('clientId')
//            ->add('projectId')
//            ->add('authUri')
//            ->add('tokenUri')
//            ->add('authProviderX509CertUrl')
//            ->add('clientSecret')
//            ->add('redirectUris')
            ->add('jsonData', TextareaType::class);
//        $builder->setDataMapper($this->dataMapper);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GoogleClient::class,
        ]);
    }
}
