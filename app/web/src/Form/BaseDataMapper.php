<?php


namespace App\Form;


use Symfony\Component\Form\Extension\Core\DataMapper\DataMapper;

class BaseDataMapper extends DataMapper
{
    public function mapDataToForms($viewData, iterable $forms): void
    {
        parent::mapDataToForms($viewData, $forms);
    }

    public function mapFormsToData(iterable $forms, &$viewData): void
    {
        parent::mapFormsToData($forms, $viewData);
    }
}
