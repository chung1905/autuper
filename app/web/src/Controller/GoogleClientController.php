<?php

namespace App\Controller;

use App\Entity\GoogleClient;
use App\Form\GoogleClientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/google/client')]
class GoogleClientController extends AbstractController
{
    #[Route('/', name: 'google_client_index', methods: ['GET'])]
    public function index(): Response
    {
        $googleClients = $this->getDoctrine()
            ->getRepository(GoogleClient::class)
            ->findAll();

        return $this->render('google_client/index.html.twig', [
            'google_clients' => $googleClients,
        ]);
    }

    #[Route('/new', name: 'google_client_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $googleClient = new GoogleClient();
        $form = $this->createForm(GoogleClientType::class, $googleClient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($googleClient);
            $entityManager->flush();

            return $this->redirectToRoute('google_client_index');
        }

        return $this->render('google_client/new.html.twig', [
            'google_client' => $googleClient,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'google_client_show', methods: ['GET'])]
    public function show(GoogleClient $googleClient): Response
    {
        return $this->render('google_client/show.html.twig', [
            'google_client' => $googleClient,
        ]);
    }

    #[Route('/{id}', name: 'google_client_delete', methods: ['DELETE'])]
    public function delete(Request $request, GoogleClient $googleClient): Response
    {
        if ($this->isCsrfTokenValid('delete' . $googleClient->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($googleClient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('google_client_index');
    }
}
