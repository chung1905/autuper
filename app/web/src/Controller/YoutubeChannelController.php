<?php

namespace App\Controller;

use App\Entity\GoogleClient;
use App\Entity\GoogleToken;
use App\Entity\YoutubeChannel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/youtube/channel')]
class YoutubeChannelController extends AbstractController
{
    #[Route('/add/{client}', name: 'youtube_channel_add', methods: ['GET'])]
    public function index(GoogleClient $client): Response
    {
        try {
            $googleClient = new \Google\Client();
            $googleClient->setAuthConfig($client->jsonData);
            $googleClient->setAccessType('offline');
            $authUrl = $googleClient->createAuthUrl([
                \Google_Service_YouTube::YOUTUBE_READONLY,
                \Google_Service_YouTube::YOUTUBE_UPLOAD,
            ]);

            return $this->render('google_client/auth.html.twig', [
                'client_id' => $client->getId(),
                'project_id' => $client->projectId,
                'auth_url' => $authUrl,
            ]);
        } catch (\Exception $e) {
            return $this->json([
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/add/{client}', name: 'youtube_channel_add_post', methods: ['POST'])]
    public function post(Request $request, GoogleClient $client): Response
    {
        try {
            $googleClient = new \Google\Client();
            $googleClient->setAuthConfig($client->jsonData);
            $googleClient->setAccessType('offline');
            $code = $request->get('code');
            $accessToken = $googleClient->fetchAccessTokenWithAuthCode($code);

            /**
             * Sample accessToken
            {
            "code": "4/1AY0e-g4raWi6SQOqpWLhvOECa_mf97W7hybyWOBlJJXZIC6ONUZ9mchby14",
            "access_token": {
                "access_token": "ya29.a0AfH6SMASObeR5hWPd5FzpNNv6n8BSxh9gj6AusDqzqJqWRogoJV1BRLdDxK_6rEdklmyRcv2tBaQ1KZgPSLQadCEkj6SZieoK1ibmbxIqqGPxbi47v9ACXjWkAdVr6nj3YhKgBjDdE_5GWZnry8QAqUql_dd",
                "expires_in": 3599,
                "refresh_token": "1//0ezEeGpVU-e1PCgYIARAAGA4SNwF-L9IrO6FKOQzPGwxw840ZnRPOihuyX3CeKurGbHn8f_fX9HmO5yzj9R9YXQPhN9xOKlvbbEM",
                "scope": "https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube.readonly",
                "token_type": "Bearer",
                "created": 1616601434
                }
            }
             */

            $channel = new YoutubeChannel();
            $channel->channelId = $code;
            $channel->channelTitle = "Untitled #" . rand(0, 1000);

            $token = new GoogleToken();
            $token->token = $accessToken['access_token'];
            $token->channel = $channel;
            $token->client = $client;
	    $token->expireAt = new \DateTime('@' . (time() + $accessToken['expires_in']));
            $token->refreshToken = $accessToken['refresh_token'];
            $token->scopes = $accessToken['scope'];

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($channel);
	    $entityManager->persist($token);
            $entityManager->flush();

            return $this->json([
                'code' => $code,
                'access_token' => $accessToken,
            ]);
        } catch (\Exception $e) {
            return $this->json([
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }
}
