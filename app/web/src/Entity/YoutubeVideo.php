<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class YoutubeVideo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\YoutubeChannel")
     */
    private $channel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Video")
     */
    private $video;

    /**
     * @ORM\Column(type="string")
     */
    private $youtubeUrl;
}
