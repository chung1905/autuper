<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class GoogleToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\YoutubeChannel")
     */
    public $channel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GoogleClient")
     */
    public $client;

    /**
     * @ORM\Column()
     */
    public $token;

    /**
     * @ORM\Column(type="datetime")
     */
    public $expireAt;

    /**
     * @ORM\Column(type="json")
     */
    public $scopes;

    /**
     * @ORM\Column()
     */
    public $refreshToken;
}
