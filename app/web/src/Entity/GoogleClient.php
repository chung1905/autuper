<?php


namespace App\Entity;

use App\Generic\SnakeCaseString;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class GoogleClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private int $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    public string $clientId;

    /**
     * @ORM\Column()
     */
    public string $projectId;

    /**
     * @ORM\Column()
     */
    public string $authUri;

    /**
     * @ORM\Column()
     */
    public string $tokenUri;

    /**
     * @ORM\Column()
     */
    public string $authProviderX509CertUrl;

    /**
     * @ORM\Column()
     */
    public string $clientSecret;

    /**
     * @ORM\Column(type="json")
     */
    public array $redirectUris;

    /**
     * @ORM\Column(type="json")
     */
    public array $jsonData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GoogleToken", mappedBy="client")
     */
    public Collection $tokenCollection;

    public function getId(): int
    {
        return $this->id;
    }

    public function setJsonData(string|array $jsonData): self
    {
        if (is_string($jsonData)) {
            $jsonData = json_decode($jsonData, true);
        }
        $installed = $jsonData['installed'];
        foreach ($installed as $key => $value) {
            $camelKey = (new SnakeCaseString($key))->toCamel();
            if (!property_exists($this, $camelKey)) {
                continue;
            }
            $this->$camelKey = $value;
        }

        $this->jsonData = $jsonData;
        return $this;
    }
}
