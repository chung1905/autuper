<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private int $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $title;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    public int $status;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $videoFilepath;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    public ?string $source;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    public ?string $sourceUrl;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    public ?string $additionalData;

    public function getId(): int
    {
        return $this->id;
    }
}
