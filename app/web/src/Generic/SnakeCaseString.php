<?php

namespace App\Generic;

class SnakeCaseString
{
    public function __construct(
        private string $str
    ) {
    }

    public function toCamel(): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $this->str))));
    }
}
