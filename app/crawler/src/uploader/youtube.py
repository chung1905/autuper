import http.client
import os
import random
import time

import httplib2
from googleapiclient.discovery import Resource
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from sqlalchemy import desc
from sqlalchemy.orm import Query

from .channel_auth import get_authenticated_service
from ..model.database import save
from ..model.video import Status, VideoRepository, Video

# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.
httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 10

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, http.client.NotConnected,
                        http.client.IncompleteRead, http.client.ImproperConnectionState,
                        http.client.CannotSendRequest, http.client.CannotSendHeader,
                        http.client.ResponseNotReady, http.client.BadStatusLine)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

ABS_VIDEO_DIR = '/mnt/'
VALID_PRIVACY_STATUSES = ("public", "private", "unlisted")


def _initialize_upload(youtube: Resource, options):
    tags = None
    if options['keywords']:
        tags = options['keywords'].split(",")

    body = dict(
        snippet=dict(
            title=options['title'],
            description=options['description'],
            tags=tags,
            categoryId=options['category']
        ),
        status=dict(
            privacyStatus=options['privacyStatus']
        )
    )

    # Call the API's videos.insert method to create and upload the video.
    insert_request = youtube.videos().insert(
        part=",".join(list(body.keys())),
        body=body,
        # The chunksize parameter specifies the size of each chunk of data, in
        # bytes, that will be uploaded at a time. Set a higher value for
        # reliable connections as fewer chunks lead to faster uploads. Set a lower
        # value for better recovery on less reliable connections.
        #
        # Setting "chunksize" equal to -1 in the code below means that the entire
        # file will be uploaded in a single HTTP request. (If the upload fails,
        # it will still be retried where it left off.) This is usually a best
        # practice, but if you're using Python older than 2.6 or if you're
        # running on App Engine, you should set the chunksize to something like
        # 1024 * 1024 (1 megabyte).
        media_body=MediaFileUpload(options['file'], chunksize=-1, resumable=True)
    )

    _resumable_upload(insert_request)


# This method implements an exponential backoff strategy to resume a
# failed upload.
def _resumable_upload(insert_request):
    response = None
    error = None
    retry = 0
    while response is None:
        try:
            print("Uploading file...")
            status, response = insert_request.next_chunk()
            if response is not None:
                if 'id' in response:
                    print("Video id '%s' was successfully uploaded." % response['id'])
                else:
                    exit("The upload failed with an unexpected response: %s" % response)
        except HttpError as e:
            if e.resp.status in RETRIABLE_STATUS_CODES:
                error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status,
                                                                     e.content)
            else:
                raise
        except RETRIABLE_EXCEPTIONS as e:
            error = "A retriable error occurred: %s" % e

        if error is not None:
            print(error)
            retry += 1
            if retry > MAX_RETRIES:
                exit("No longer attempting to retry.")

            max_sleep = 2 ** retry
            sleep_seconds = random.random() * max_sleep
            print("Sleeping %f seconds and then retrying..." % sleep_seconds)
            time.sleep(sleep_seconds)


def upload(qty: int = 1):
    youtube: Resource = get_authenticated_service()

    un_uploaded: Query = VideoRepository.find_by_status(Status.DOWNLOADED).order_by(desc(Video.id))
    if qty > 0:
        un_uploaded = un_uploaded.limit(qty)

    for video in un_uploaded:
        args = {
            'file': os.path.join(ABS_VIDEO_DIR, video.video_filepath),
            'title': video.title,
            'description': 'Thanks for watching',
            'category': 22,
            'keywords': 'tiktok,comedy,funny',
            'privacyStatus': VALID_PRIVACY_STATUSES[0]
        }
        try:
            _initialize_upload(youtube, args)
            video.status = Status.UPLOADED
            message = "{}: Uploaded {}".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), video.title)
            print(message)
        except HttpError as e:
            video.status = Status.UPLOAD_ERR
            message = "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)
            print(message)
        save(video)
