from typing import Tuple

from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
from googleapiclient.discovery import build, Resource
from ..model.database import save
from ..model.google_client import GoogleClient, GoogleClientRepository
from ..model.google_token import GoogleToken, GoogleTokenRepository
from ..model.youtube_channel import YoutubeChannel, YoutubeChannelRepository

# This OAuth 2.0 access scope allows an application to upload files to the
# authenticated user's YouTube channel, but doesn't allow other types of access.
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_READONLY_SCOPE = "https://www.googleapis.com/auth/youtube.readonly"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

GOOGLE_CLIENT_ID = '811050273320-mrlkl5mf2fbl4vbvibcevmshg2dal5at.apps.googleusercontent.com'


def get_authenticated_service(channel_id: int = None) -> Resource:
    token = _query_old_token(GOOGLE_CLIENT_ID)
    credentials: Credentials = _populate_credentials(token)

    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, credentials=credentials)


def add_channel(client_id: str) -> YoutubeChannel:
    credentials, client = _get_new_credentials(client_id=client_id)
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, credentials=credentials)

    channels_list = youtube.channels().list(part='id,brandingSettings', mine=True).execute()
    channel_data = channels_list['items'][0]

    channel_entity = _get_or_create_channel(channel_data)
    save(_create_token(credentials, channel_entity, client))

    return channel_entity


def _populate_credentials(token: GoogleToken) -> Credentials:
    return Credentials(
        token=token.token,
        refresh_token=token.refresh_token,
        token_uri=token.client.token_uri,
        client_id=token.client.client_id,
        client_secret=token.client.client_secret,
    )


def _query_old_token(client_id: str, channel_id: int = None) -> GoogleToken:
    """
    TODO: Support multiple channel, for now, only one channel is supported

    Consider to use client_id as int instead of string.
    String: this is real channel ID from youtube, always fixed => require join
    Int: this is channel ID from database, not fixed => better in GUI
    """
    return GoogleTokenRepository.find_one_by_google_client_id(client_id)


def _get_new_credentials(client_id: str = None) -> Tuple[Credentials, GoogleClient]:
    client = GoogleClientRepository.find_one_by_google_client_id(client_id)
    flow: Flow = Flow.from_client_config(
        client_config=client.json_data,
        scopes=[YOUTUBE_READONLY_SCOPE, YOUTUBE_UPLOAD_SCOPE]
    )

    flow.redirect_uri = InstalledAppFlow._OOB_REDIRECT_URI
    auth_url, state = flow.authorization_url()
    print(auth_url)

    code = input('code:')
    flow.fetch_token(code=code)
    credentials: Credentials = flow.credentials

    return credentials, client


def _get_or_create_channel(data) -> YoutubeChannel:
    channel_id = data['id']
    channel = YoutubeChannelRepository.find_one_or_none_by_channel_id(channel_id)
    if channel is None:
        channel_title = data['brandingSettings']['channel']['title']
        channel = YoutubeChannel(
            channel_id=channel_id,
            channel_title=channel_title,
        )
        save(channel)

    return channel


def _create_token(credentials: Credentials, channel: YoutubeChannel, client: GoogleClient) -> GoogleToken:
    return GoogleToken(
        client=client,
        channel=channel,
        token=credentials.token,
        expire_at=credentials.expiry,
        scopes=credentials.scopes,
        refresh_token=credentials.refresh_token,
    )
