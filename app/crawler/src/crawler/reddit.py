import json
from typing import List

import requests
from sqlalchemy.exc import DataError
from ..model.database import save
from ..model.video import Video, Status, VideoRepository


class RedditCrawler:
    headers = {
        'User-Agent': 'A.Y. User Agent 1.0',
    }

    subreddits = (
        'funny',
        'ContagiousLaughter',
        'oddlysatisfying',
        'nonononoyes',
        'yesyesyesno',
        'PublicFreakout',
        'blackmagicfuckery',
        'Unexpected',
        'TikTokCringe',
    )

    def crawl(self, qty_total: int = 0, qty_per_subreddit: int = 100):
        count_total = 0
        while qty_total == 0 or count_total < qty_total:
            for s in self.subreddits:
                count_sub = 0
                after: str = ''
                while count_sub < qty_per_subreddit:
                    url = 'https://www.reddit.com/r/{0}/hot.json?limit={1}'.format(s, max(qty_per_subreddit, 100))
                    if after:
                        url += '&after=' + after
                    raw_resp = requests.get(url, headers=self.headers)
                    resp = RedditResponse(raw_resp.json())
                    qty = resp.length()
                    if qty == 0:
                        break
                    after = resp.last_item()
                    count_total += qty
                    count_sub += qty
                    print('Crawled {0} from r/{1}. Total: {2}/{3}'.format(qty, s, count_total, qty_total))


class RedditResponse:
    source = 'reddit'

    def __init__(self, data: dict):
        self.json = data
        self.videos = self.parse_and_save_data()

    def parse_and_save_data(self) -> List[Video]:
        videos: List[Video] = []
        for video in self._get_videos():
            data = video['data']
            source_url = data['url_overridden_by_dest']
            found = VideoRepository.find_by_source_url(source_url)
            if found is not None:
                continue
            title = data['title'][:70]
            status = Status.INITIALIZED
            source = RedditResponse.source + '/' + data['subreddit'] + '/' + data['name']
            v = Video(
                title=title,
                status=status,
                source_url=source_url,
                source=source,
                additional_data=json.dumps(video),
            )
            try:
                save(v)
                videos.append(v)
            except DataError:
                print('Error when save {0}'.format(title))
        return videos

    def length(self) -> int:
        return len(self.videos)

    def _get_video_urls(self):
        for i in self._get_videos():
            yield i['data']['url_overridden_by_dest']

    def _get_videos(self):
        for i in self._children():
            if i['data']['is_video']:
                yield i

    def _children(self):
        return self.json['data']['children']

    def last_item(self) -> str:
        return self.json['data']['after']
