from typing import Union, List

import youtube_dl
from sqlalchemy import desc
from sqlalchemy.orm import Query
from youtube_dl import YoutubeDL
from ..model.database import save
from ..model.video import Video, Status, VideoRepository

DOWNLOAD_DIR = '/mnt/'


def download(qty: int = 0):
    ydl = __get_youtube_downloader()
    videos: Query = VideoRepository.find_by_status(Status.INITIALIZED).order_by(desc(Video.id))
    if qty > 0:
        videos = videos.limit(qty)
    _download_multi(videos, ydl)


def _download_multi(videos: Union[List[Video], Query], ydl: YoutubeDL):
    for video in videos:
        _download_one(video, ydl)


def _download_one(video: Video, ydl: YoutubeDL):
    source = video.source.split('/')[0]
    ydl.params['outtmpl'] = DOWNLOAD_DIR + 'data/{0}/%(title)s-%(id)s.%(ext)s'.format(source)
    url = video.source_url
    info = ydl.extract_info(url, download=False)
    try:
        ydl.process_info(info)
        video.status = Status.DOWNLOADED
        video.video_filepath = info['_filename'][len(DOWNLOAD_DIR):]
    except:
        video.status = Status.DOWNLOAD_ERR
        pass
    save(video)


def __get_youtube_downloader() -> YoutubeDL:
    return youtube_dl.YoutubeDL()
