import sys

if __name__ == "__main__":
    action = sys.argv[1:][0]

    if action == 'crawl':
        from .crawler.reddit import RedditCrawler

        qty = int(sys.argv[1:][1])
        reddit = RedditCrawler()
        reddit.crawl(qty)
    elif action == 'download':
        from .downloader.youtube_dl import download

        qty = int(sys.argv[1:][1])
        download(qty)
    elif action == 'upload':
        from .uploader.youtube import upload

        qty = int(sys.argv[1:][1])
        upload(qty)
    elif action == 'add-channel':
        from .uploader.channel_auth import add_channel, GOOGLE_CLIENT_ID

        add_channel(GOOGLE_CLIENT_ID)
    else:
        print('Please select an action: crawl, upload (qty) or add-channel.')
