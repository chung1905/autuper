import json

import requests
from requests import RequestException


def send(title: str, body: str):
    url = "http://192.168.1.201:8080/email"
    payload = {
        "title": title,
        "body": body,
    }
    headers = {
        'Content-Type': 'application/json'
    }

    try:
        response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    except RequestException as e:
        print("Notify RequestException: {}".format(e))
    else:
        print(response.text)
