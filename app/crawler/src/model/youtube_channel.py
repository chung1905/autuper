from typing import Optional

from sqlalchemy import Column, String, Integer
from .database import Base, _session


class YoutubeChannel(Base):
    __tablename__ = 'youtube_channel'
    id = Column(Integer, primary_key=True)
    channel_id = Column(String, unique=True)
    channel_title = Column(String)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class YoutubeChannelRepository:
    @staticmethod
    def find_one_or_none_by_channel_id(channel_id: str) -> Optional[YoutubeChannel]:
        return _session().query(YoutubeChannel).filter(YoutubeChannel.channel_id == channel_id).one_or_none()
