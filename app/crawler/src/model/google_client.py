from sqlalchemy import Column, String, Integer, JSON

from .database import Base, _session


class GoogleClient(Base):
    __tablename__ = 'google_client'
    id = Column(Integer, primary_key=True)
    client_id = Column(String, unique=True)
    client_secret = Column(String)
    token_uri = Column(String)
    json_data = Column(JSON)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class GoogleClientRepository:
    @staticmethod
    def find_one_by_google_client_id(client_id: str) -> GoogleClient:
        return _session().query(GoogleClient).filter(GoogleClient.client_id == client_id).one()
