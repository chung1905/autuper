from sqlalchemy import Column, String, DateTime, Integer, JSON, ForeignKey
from sqlalchemy.orm import relationship

from .database import Base, _session
from .google_client import GoogleClient
from .youtube_channel import YoutubeChannel


class GoogleToken(Base):
    __tablename__ = 'google_token'
    id = Column(Integer, primary_key=True)
    client_id = Column(Integer, ForeignKey('google_client.id'))
    channel_id = Column(Integer, ForeignKey('youtube_channel.id'))
    token = Column(String)
    expire_at = Column(DateTime)
    scopes = Column(JSON)
    refresh_token = Column(String)

    client: GoogleClient = relationship(GoogleClient)
    channel: YoutubeChannel = relationship(YoutubeChannel)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class GoogleTokenRepository:
    @staticmethod
    def find_one_by_google_client_id(client_id: str) -> GoogleToken:
        return _session().query(GoogleToken).filter(GoogleClient.client_id == client_id).one()
