from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

Base = declarative_base()

__engine = create_engine('postgresql://postgres:postgres@db/postgres')
__session_maker = sessionmaker(bind=__engine)
__session = __session_maker()


def _session() -> Session:
    return __session


def save(entity: Base) -> None:
    session = _session()
    session.add(entity)
    session.commit()
