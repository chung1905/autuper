from enum import IntEnum
from typing import Optional

from sqlalchemy import Column, Integer, String, Text, SmallInteger
from sqlalchemy.orm import Query
from .database import Base, _session


class Video(Base):
    __tablename__ = 'video'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    video_filepath = Column(String)
    status = Column(SmallInteger)
    source = Column(String, unique=True)
    source_url = Column(String, unique=True)
    additional_data = Column(Text)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Status(IntEnum):
    INITIALIZED = 0
    DOWNLOADED = 1
    UPLOADED = 2  # To remove
    DOWNLOAD_ERR = 3
    TO_UPLOAD = 4
    UPLOAD_ERR = 5


class VideoRepository:
    @staticmethod
    def find_by_status(status: Status) -> Query:
        return _session().query(Video).filter(Video.status == status)

    @staticmethod
    def find_by_source_url(url: str) -> Optional[Video]:
        return _session().query(Video).filter(Video.source_url == url).one_or_none()
