# Auto Youtuber

___

## TODO:

- Crawler
  - Download x videos per day (from more source) (WIP)
- Set nullable=false for some video's column (eg. source, source_url, title)
- Uploader
  - Support multiple client & channel
  - Save which videos uploaded to which channels
  - Update channel name
- Cron
  - Clean old backups
- declare(strict_types=1);
- GUI
  - Custom video title (by admin)
  - Customize video information
  - Upload priority
- Post-process download videos
  - Download gif, then convert to mp4
- Finish "Manual Guide"
- Copyright issue
  - Upload to temp channel first
- Email notification (WIP)
  - Using [notifier](https://gitlab.com/chung1905/notifier/)
  - Currently, fixed URL, update later

___

## How to use

##### Please refer to Makefile at the moment

### Add new Google Client
- Create new Google Project: https://console.developers.google.com/projectcreate
- Enable YoutubeAPI: https://console.developers.google.com/apis/library/youtube.googleapis.com
- Create new Oauth2 Credentials: https://console.developers.google.com/apis/credentials/oauthclient
- Select type: Desktop App (maybe support more type (web type) in future)
- Download credentials file (in `.json` fomat)
- Add file content `jsonData` field

___

## Post-mortem

###### (after first major changes)

### Database

#### 1. The system/engine

At the beginning, this project use SQLite, which provides many pro:

- Very fast integration.
- Back up easily (just scp/rsync from server to local)

But:

- I found it's easy to error when crawling/(view from adminer)/uploading at the same time.
- Hard to change column
  type/name ([require dropping the database](https://stackoverflow.com/questions/21199398/sqlite-alter-a-tables-column-type)
  ?).
- After prune data, requires "vacuum" to reduce file size.

Then, I change it to PostgreSQL, which can fix all the cons. Some notes:

- It that easy to set up, but it can fix by coming to Docker.
- Backup is simple with a Makefile, maybe I should upgrade to Docker/cron to run backup automatically.
- Migration can be simple
  with [Jetbrains' database tool](https://www.jetbrains.com/help/idea/database-tool-window.html#controls_on_toolbar).

#### 2. Schema management

- First, with SQLAlchemy, I can create a simple table schema very fast.

  Then I have to use migrations tool for update schema in future (which is very good practice).

  But I prefer Symfony's doctrine:schema:update, it's not recommended in production btw (because
  it's [incredibly powerful](https://symfony.com/doc/3.3/doctrine.html#creating-the-database-tables-schema)).

- Doctrine will add complexity into application, but maybe I will need this later to do a web interface.

___
